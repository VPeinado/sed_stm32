/*
 * humedad.h
 *
 *  Created on: Dec 28, 2020
 *      Author: Luis Quintana
 */

#ifndef INC_HUMEDAD_H_
#define INC_HUMEDAD_H_



uint8_t getHumedad();
void ControlHumedad(uint8_t umbral_humedad);



#endif /* INC_HUMEDAD_H_ */
