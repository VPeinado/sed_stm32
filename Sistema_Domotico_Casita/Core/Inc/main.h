/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SONDA_TEMP_Pin GPIO_PIN_2
#define SONDA_TEMP_GPIO_Port GPIOE
#define CALEFACCION_Pin GPIO_PIN_3
#define CALEFACCION_GPIO_Port GPIOE
#define SENSOR_SUBIDA_Pin GPIO_PIN_0
#define SENSOR_SUBIDA_GPIO_Port GPIOC
#define SENSOR_SUBIDA_EXTI_IRQn EXTI0_IRQn
#define SENSOR_BAJADA_Pin GPIO_PIN_1
#define SENSOR_BAJADA_GPIO_Port GPIOC
#define SENSOR_BAJADA_EXTI_IRQn EXTI1_IRQn
#define LDR_SENSOR_Pin GPIO_PIN_3
#define LDR_SENSOR_GPIO_Port GPIOC
#define ENC_LUZ_CLK_Pin GPIO_PIN_1
#define ENC_LUZ_CLK_GPIO_Port GPIOA
#define SW_OLED_Pin GPIO_PIN_4
#define SW_OLED_GPIO_Port GPIOA
#define SW_OLED_EXTI_IRQn EXTI4_IRQn
#define SW_LUZ_Pin GPIO_PIN_5
#define SW_LUZ_GPIO_Port GPIOA
#define SW_LUZ_EXTI_IRQn EXTI9_5_IRQn
#define PWM_LUZ_Pin GPIO_PIN_6
#define PWM_LUZ_GPIO_Port GPIOA
#define BOTON_PERSIANA_Pin GPIO_PIN_7
#define BOTON_PERSIANA_GPIO_Port GPIOA
#define BOTON_PERSIANA_EXTI_IRQn EXTI9_5_IRQn
#define WATER_SENSOR_Pin GPIO_PIN_0
#define WATER_SENSOR_GPIO_Port GPIOB
#define ENC_OLED_DT_Pin GPIO_PIN_9
#define ENC_OLED_DT_GPIO_Port GPIOE
#define ENC_OLED_CLK_Pin GPIO_PIN_11
#define ENC_OLED_CLK_GPIO_Port GPIOE
#define RELOJ_SCL_Pin GPIO_PIN_10
#define RELOJ_SCL_GPIO_Port GPIOB
#define RELOJ_SDA_Pin GPIO_PIN_11
#define RELOJ_SDA_GPIO_Port GPIOB
#define M_PERSIANA_IN2_Pin GPIO_PIN_10
#define M_PERSIANA_IN2_GPIO_Port GPIOD
#define M_PERSIANA_IN1_Pin GPIO_PIN_11
#define M_PERSIANA_IN1_GPIO_Port GPIOD
#define PWM_VENTILADOR_Pin GPIO_PIN_12
#define PWM_VENTILADOR_GPIO_Port GPIOD
#define ENC_LUZ_DT_Pin GPIO_PIN_15
#define ENC_LUZ_DT_GPIO_Port GPIOA
#define BOMBA_AGUA_Pin GPIO_PIN_4
#define BOMBA_AGUA_GPIO_Port GPIOD
#define OLED_SCL_Pin GPIO_PIN_6
#define OLED_SCL_GPIO_Port GPIOB
#define OLED_SDA_Pin GPIO_PIN_7
#define OLED_SDA_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
