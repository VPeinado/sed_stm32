/*
 * luz.h
 *
 *  Created on: Dec 22, 2020
 *      Author: Victor Peinado
 */

#ifndef INC_LUZ_H_
#define INC_LUZ_H_

void LightControl(volatile uint8_t*, volatile uint16_t*);
void CallbackLight(volatile uint8_t*, volatile uint16_t*);

#endif /* INC_LUZ_H_ */
