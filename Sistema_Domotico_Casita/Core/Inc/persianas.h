/*
 * persianas.h
 *
 *  Created on: Dec 28, 2020
 *      Author: Luis Quintana
 */

#ifndef INC_PERSIANAS_H_
#define INC_PERSIANAS_H_

uint8_t getLuminosidad();
void ControlPersiana(uint8_t * h_actual, uint8_t * h_apertura, uint8_t * h_cierre, uint8_t u_apetura, uint8_t u_cierre, int sel_control, int estado_per, int accion_motor);
void CallbackPersianas(int *estado_per,int *accion_motor, uint16_t);




#endif /* INC_PERSIANAS_H_ */
