/*
 * temperatura.h
 *
 *  Created on: Jan 19, 2021
 *      Author: peina
 */

#ifndef INC_TEMPERATURA_H_
#define INC_TEMPERATURA_H_

#include "ds18b20_mflib.h"

void ControlTemperatura(float* temp, uint8_t setcal);

#endif /* INC_TEMPERATURA_H_ */
