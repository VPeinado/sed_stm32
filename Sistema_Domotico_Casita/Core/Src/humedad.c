/*
 * humedad.c
 *
 *  Created on: Dec 28, 2020
 *      Author: Luis Quintana
 */
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_adc.h"
#include "stm32f4xx_it.h"

//Cambiar según el pin donde se encuentre el relé de la bomba
#define bomba_port GPIOD
#define bomba_pin GPIO_PIN_4

//Cambiar en función del conversor que se utilice
ADC_HandleTypeDef hadc2;

/* 			CAMBIAR SOLO HADC			*/

#include "humedad.h"


uint8_t getHumedad()
{
	uint8_t humedad;
	HAL_ADC_Start(&hadc2);
	if (HAL_ADC_PollForConversion(&hadc2,1000)==HAL_OK)
	{
		humedad=HAL_ADC_GetValue(&hadc2);
	}
	HAL_ADC_Stop(&hadc2);

	return humedad;
}

void ControlHumedad(uint8_t umbral_humedad)
{
	if(umbral_humedad >= getHumedad())
	{
	  HAL_GPIO_WritePin(bomba_port, bomba_pin, 1);
	}
	else
	  HAL_GPIO_WritePin(bomba_port, bomba_pin, 0);
}
