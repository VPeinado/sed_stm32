/*
 * temperatura.c
 *
 *  Created on: Jan 19, 2021
 *      Author: peina
 */
#include "temperatura.h"

void ControlTemperatura (float* temp, uint8_t setcal)
{
	ds18b20_init_seq();
	ds18b20_send_rom_cmd(SKIP_ROM_CMD_BYTE);
	ds18b20_send_function_cmd(CONVERT_T_CMD);

	//delay_us(100);
	HAL_Delay(0.1);	//100us

	ds18b20_init_seq();
	ds18b20_send_rom_cmd(SKIP_ROM_CMD_BYTE);
	ds18b20_send_function_cmd(READ_SCRATCHPAD_CMD);
	*temp = ds18b20_read_temp();	// returns float value

	if((uint8_t)*temp < setcal) // Ver conversion de float a uint_8t o viceversa
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, 1);
	else
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, 0);
}
