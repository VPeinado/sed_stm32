/*
 * luz.c
 *
 *  Created on: Dec 22, 2020
 *      Author: Victor Peinado
 */

#include "stm32f4xx_hal.h";
#include "luz.h"

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;

/******************** MODIFICAR INTENSIDAD DE LA LUZ ********************/
void LightControl(volatile uint8_t* luz, volatile uint16_t* LED_DutyCycle)
{
	  if(*luz==1)
	  {
		  if(TIM2->CNT > 50 && TIM2->CNT < 450)
			  *LED_DutyCycle = ((TIM2->CNT));
		  else if(TIM2->CNT <= 50)
		  {
			  TIM2->CNT = 50;
			  *LED_DutyCycle = 0;
		  }
		  else if(TIM2->CNT >= 450)
		  {
			  TIM2->CNT = 450;
			  *LED_DutyCycle = 500;
		  }
	  }
	  TIM3->CCR1 = *LED_DutyCycle;
	  HAL_Delay(20);
}

/******************** ENCENDER O APAGAR LA LUZ ********************/
void CallbackLight(volatile uint8_t* luz, volatile uint16_t* LED_DutyCycle)
{
	if(*luz == 1)
	{
		*LED_DutyCycle = 0;
		*luz = 0;
		HAL_TIM_Encoder_Stop(&htim2, TIM_CHANNEL_ALL);
	}
	else
	{
		*luz = 1;
		HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_ALL);
		TIM2->CNT = 500/2;
	}
}
