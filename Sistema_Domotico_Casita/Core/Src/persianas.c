/*
 * persianas.c
 *
 *  Created on: Dec 28, 2020
 *      Author: Luis Quintana
 */

#include"stm32f4xx_hal.h"
#include"stm32f4xx_hal_adc.h"

/* MODOS DE CONTROL:
 * flag == 1 -> Modo Control por Luminosidad
 * flag == 0 -> Modo Control por Horas
 */
// MOTOR BAJAR
#define bajar_port GPIOD
#define bajar_pin GPIO_PIN_11

// MOTOR SUBIR
#define subir_port GPIOD
#define subir_pin GPIO_PIN_10

// LDR -> 0 a 255 siendo 255 negro
ADC_HandleTypeDef hadc3;



#include "persianas.h"


uint8_t getLuminosidad()
{
	uint8_t luminosidad;
	HAL_ADC_Start(&hadc3);
	if (HAL_ADC_PollForConversion(&hadc3,1000)==HAL_OK)
	{
		luminosidad=HAL_ADC_GetValue(&hadc3);
	}
	HAL_ADC_Stop(&hadc3);

	return luminosidad;
}


void ControlPersiana(uint8_t * h_actual, uint8_t * h_apertura, uint8_t * h_cierre,uint8_t u_apertura, uint8_t u_cierre, int sel_control, int estado_per, int accion_motor)
{

	if(sel_control == 2)//Control por luminosidad
	{
		if(u_cierre < getLuminosidad() && estado_per == 1) //Si esta subida bajarla (noche)
		{
			HAL_GPIO_WritePin(bajar_port, bajar_pin, 1);
			HAL_GPIO_WritePin(subir_port, subir_pin, 0);
		}
		else if(u_apertura >= getLuminosidad() && estado_per == 0)// si esta baja subirla (dia)
		{
			HAL_GPIO_WritePin(subir_port, subir_pin, 1);
			HAL_GPIO_WritePin(bajar_port, bajar_pin, 0);
		}
		else
		{
			HAL_GPIO_WritePin(subir_port, subir_pin, 0);
			HAL_GPIO_WritePin(bajar_port, bajar_pin, 0);
		}
	}
	else if(sel_control == 1)	//Control por hora
	{
		if(h_actual[0] == h_cierre[0] && h_actual[1] == h_cierre[1]  && estado_per == 1)
		{
			HAL_GPIO_WritePin(bajar_port, bajar_pin, 1);
			HAL_GPIO_WritePin(subir_port, subir_pin, 0);
		}
		else if(h_actual[0] == h_apertura[0] && h_actual[1] == h_apertura[1] && estado_per == 0)
		{
			HAL_GPIO_WritePin(subir_port, subir_pin, 1);
			HAL_GPIO_WritePin(bajar_port, bajar_pin, 0);
		}
		else
		{
			HAL_GPIO_WritePin(subir_port, subir_pin, 0);
			HAL_GPIO_WritePin(bajar_port, bajar_pin, 0);
		}
	}
	else if(sel_control == 0)	//CONTROL MANUAL
	{
		if(accion_motor == 1)
		{
			HAL_GPIO_WritePin(subir_port, subir_pin, 1);
			HAL_GPIO_WritePin(bajar_port, bajar_pin, 0);
		}
		else if(accion_motor == -1)
		{
			HAL_GPIO_WritePin(subir_port, subir_pin, 0);
			HAL_GPIO_WritePin(bajar_port, bajar_pin, 1);
		}
		else if(accion_motor == 0)
		{
			HAL_GPIO_WritePin(subir_port, subir_pin, 0);
			HAL_GPIO_WritePin(bajar_port, bajar_pin, 0);
		}
	}
}

void CallbackPersianas0(int * estado_per, int* accion_motor)
{
		*estado_per = 0;
		*accion_motor = 0;
}
void CallbackPersianas1(int * estado_per, int* accion_motor)
{
	*estado_per = 1;
	*accion_motor = 0;
}
void CallbackPersianas2(int * estado_per, int* accion_motor)
{
	if(*estado_per == 1)
		*accion_motor = -1;
	else if(*estado_per == 0)
		*accion_motor = 1;
}
