/*
 * menu.c
 *
 *  Created on: Dec 18, 2020
 *      Author: Victor Peinado
 */

#include "stm32f4xx_hal.h"
#include "menu.h"
#include "fonts.h"
#include "ssd1306.h"
#include "ds1307.h"
#include "stdio.h"
#include "string.h"

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim5;

uint32_t standby = 0;
uint32_t tick_anterior = 0;
uint32_t tiempo_anterior = 0;

/******************** DIBUJAR LA FLECHA EN LOS MENUS DESEADOS ********************/
void DrawArrow(volatile uint8_t menu)
{
	if(menu == 100 || menu == 21 || menu == 31 || menu == 52 || menu == 53 || menu == 61)
	{
	  	  SSD1306_GotoXY (0, 16);
	  	  SSD1306_DrawFilledRectangle(0, 0, 7, 36, SSD1306_COLOR_BLACK);
	}
	else
	{
		switch(TIM1->CNT){	//MOVIMIENTO FLECHITA
		  case 0: case 16:
		  	  SSD1306_GotoXY (0, 16);
		  	  SSD1306_Puts (">", &Font_7x10, 1);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
			  break;
		  case 4: case 20:
		  	  SSD1306_GotoXY (0, 28);
		  	  SSD1306_Puts (">", &Font_7x10, 1);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
			  break;
		  case 8:
		  	  SSD1306_GotoXY (0, 40);
		  	  SSD1306_Puts (">", &Font_7x10, 1);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
			  break;
		  case 12:
		  	  SSD1306_GotoXY (0, 52);
		  	  SSD1306_Puts (">", &Font_7x10, 1);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
			  break;
		  }
	}
}

/******************** DEVOLVER LA FLECHA A TICK CORRECTO DESPUES DE UN TIEMPO ********************/
void DebounceTick(volatile uint8_t menu)
{
	int resto = TIM1->CNT % 4;
	int cociente = TIM1->CNT / 4;
	if (resto == 1 || resto == 2 || resto == 3){
	  if (HAL_GetTick() - tiempo_anterior > 700){	//Periodo antirrebotes
		  if (resto == 1 || resto == 2){
			  TIM1->CNT = 4 * cociente;
			  tiempo_anterior = HAL_GetTick();
		  }
		  else if (resto == 3)	//|| resto == 2, al estar en medio se puede cambiar
		  {
			  TIM1->CNT = 4 * cociente + 4;
			  tiempo_anterior = HAL_GetTick();
		  }
		  if (TIM1->CNT > TIM1->ARR){
			  TIM1->CNT = 0;
			  tiempo_anterior = HAL_GetTick();
		  }
	  }
	}
	else
		  tiempo_anterior = HAL_GetTick();
}

/******************** TIEMPO DE STANDBY ********************/
void Standby(volatile uint8_t* menu)
{
	if (tick_anterior == TIM1->CNT)
	{
		if (HAL_GetTick() - standby > 5000)	//Periodo standby
		{
			*menu = 100;
			  TIM1->ARR = 0;
			standby = HAL_GetTick();
		}
	}
	else
	{
		standby = HAL_GetTick();
	}
	tick_anterior = TIM1->CNT;
}

/******************** DIBUJAR EL MENU ********************/
void DrawMenu(volatile uint8_t* menu, uint8_t* rtc_info,uint8_t* h_cierre, uint8_t* h_apertura, float temp, uint8_t* setcal, uint8_t* setvent, uint8_t* sethum, uint8_t* setmodo, uint8_t setalarm)
{
	  SSD1306_DrawFilledRectangle(0, 0, 127, 63, SSD1306_COLOR_BLACK);	//REFRESCA LA PANTALLA SIN CLEAR

	  DebounceTick(*menu);	//LLAMADA POR SI TICK INCORRECTO
	  DrawArrow(*menu);	//LLAMADA PARA DIBUJAR LA FLECHITA

	  char buffer[50];
	  char dia[15];

	if(*menu == 100)	//MENU STANDBY
	{
		switch(rtc_info[3]) //TRANSFORMACION DE week_day EN STRING
		{
			case 1:
				sprintf(dia, "   Lunes");
				break;
			case 2:
				sprintf(dia, "  Martes");
				break;
			case 3:
				sprintf(dia, " Miercoles");
				break;
			case 4:
				sprintf(dia, "  Jueves");
				break;
			case 5:
				sprintf(dia, "  Viernes");
				break;
			case 6:
				sprintf(dia, "  Sabado");
				break;
			case 7:
				sprintf(dia, "  Domingo");
				break;
			default:
				sprintf(dia, "ERROR");
				break;
		}
	  	  SSD1306_GotoXY (18, 0);
		  sprintf(buffer, "%02d:%02d:%02d", rtc_info[0], rtc_info[1], rtc_info[2]);
	  	  SSD1306_Puts (buffer, &Font_11x18, 1);
	  	  SSD1306_GotoXY (0, 25);
	  	  SSD1306_Puts (dia, &Font_11x18, 1);
	  	  SSD1306_GotoXY (8, 45);
		  sprintf(buffer, "%02d/%02d/20%02d", rtc_info[4], rtc_info[5], rtc_info[6]);
	  	  SSD1306_Puts (buffer, &Font_11x18, 1);
	  	  SSD1306_UpdateScreen();
	}
	else if(*menu == 0)	//MENU PRINCIPAL
		  {
			if (TIM1->CNT <= 12)
			{
			  	  SSD1306_GotoXY (0,0);
			  	  SSD1306_Puts ("PRINCIPAL", &Font_11x18, 1);
			  	  SSD1306_GotoXY (7, 16);
			  	  SSD1306_Puts ("Temperatura", &Font_7x10, 1);
			  	  SSD1306_GotoXY (7, 28);
			  	  SSD1306_Puts ("Calefaccion", &Font_7x10, 1);
			  	  SSD1306_GotoXY (7, 40);
			  	  SSD1306_Puts ("Ventilador", &Font_7x10, 1);
			  	  SSD1306_GotoXY (7, 52);
			  	  SSD1306_Puts ("Alarma", &Font_7x10, 1);
			  	  SSD1306_GotoXY (106, 7);
			  	  SSD1306_Puts ("1/2", &Font_7x10, 1);
			  	  SSD1306_UpdateScreen();
			}
			else if (TIM1->CNT > 12)
			{
			  	  SSD1306_GotoXY (0,0);
			  	  SSD1306_Puts ("PRINCIPAL", &Font_11x18, 1);
			  	  SSD1306_GotoXY (7, 16);
			  	  SSD1306_Puts ("Persianas", &Font_7x10, 1);
			  	  SSD1306_GotoXY (7, 28);
			  	  SSD1306_Puts ("Riego", &Font_7x10, 1);
			  	  SSD1306_GotoXY (106, 7);
			  	  SSD1306_Puts ("2/2", &Font_7x10, 1);
			  	  SSD1306_UpdateScreen();
			}

			  Standby(menu);
		  }
	else if(*menu == 1)	//MENU TEMPERATURA
		  {
			  sprintf(buffer, "%01f'C", temp);
		  	  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("TEMPERATURA", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Atras", &Font_7x10, 1);
		  	  SSD1306_GotoXY (35, 32);
		  	  SSD1306_Puts (buffer, &Font_16x26, 1);
		  	  SSD1306_UpdateScreen();
		  }
	else if(*menu == 2)	//MENU CALEFACCION
		  {
			  sprintf(buffer, "%d'C", *setcal);
		  	  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("CALEFACCION", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Atras", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 28);
		  	  SSD1306_Puts ("Cambiar", &Font_7x10, 1);
		  	  SSD1306_GotoXY (42, 40);
		  	  SSD1306_Puts (buffer, &Font_11x18, 1);
		  	  SSD1306_UpdateScreen();
		  }
	else if(*menu == 21)	//SELECCION CALEFACCION
		  {
			  sprintf(buffer, "%d'C", (int)(TIM1->CNT)/4);
			  *setcal = (TIM1->CNT)/4;
		  	  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("CALEFACCION", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Pulse para", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 25);
		  	  SSD1306_Puts ("confirmar", &Font_7x10, 1);
		  	  SSD1306_GotoXY (35, 37);
		  	  SSD1306_Puts (buffer, &Font_16x26, 1);
		  	  SSD1306_UpdateScreen();
		  }
	else if(*menu == 3)	//MENU VENTILADOR
		  {
			  sprintf(buffer, "%d rpm", *setvent*10);
		  	  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("VENTILADOR", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Atras", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 28);
		  	  SSD1306_Puts ("Cambiar", &Font_7x10, 1);
		  	  SSD1306_GotoXY (32, 40);
		  	  SSD1306_Puts (buffer, &Font_11x18, 1);
		  	  SSD1306_UpdateScreen();
		  }
	else if(*menu == 31)	//SELECCION VENTILADOR
		  {
			  sprintf(buffer, "%d rpm", (int)((TIM1->CNT)/4)*10);
			  *setvent = (TIM1->CNT)/4;
			  TIM4->CCR1 = *setvent;
			  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("VENTILADOR", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Pulse para", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 25);
		  	  SSD1306_Puts ("confirmar", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 37);
		  	  SSD1306_Puts (buffer, &Font_16x26, 1);
		  	  SSD1306_UpdateScreen();
		  }
	else if(*menu == 4)	//MENU ALARMA
		  {
		  	  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("  ALARMA", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Atras", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 28);
		  	  SSD1306_Puts ("Cambiar", &Font_7x10, 1);
		  	  SSD1306_GotoXY (3, 45);
		  	  if (setalarm == 0)
		  	  {
		  		  SSD1306_Puts ("DESACTIVADA", &Font_11x18, 1);
		  		  HAL_TIM_PWM_Stop(&htim5, TIM_CHANNEL_1);
		  	  }
		  	  else
		  	  {
		  		  SSD1306_Puts (" ACTIVADA", &Font_11x18, 1);
		  		  HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_1);
		  		  //__HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_1, 1000);
		  	  }
		  	  SSD1306_UpdateScreen();
		  }
	else if(*menu == 5)	//MENU PERSIANA
		  {
		  	  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts (" PERSIANA", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Atras", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 28);
		  	  SSD1306_Puts ("Cambiar modo", &Font_7x10, 1);
		  	  if (*setmodo == 0){
			  	  SSD1306_GotoXY (7, 52);
		  		  SSD1306_Puts ("     MANUAL", &Font_7x10, 1);
		  	  }
		  	  else if (*setmodo == 1){
			  	  SSD1306_GotoXY (7, 52);
		  		  SSD1306_Puts (" POR LUMINOSIDAD", &Font_7x10, 1);
			  	  SSD1306_GotoXY (7, 40);
		  		  SSD1306_Puts ("Cambiar umbral", &Font_7x10, 1);
		  	  }
		  	  else{
			  	  SSD1306_GotoXY (7, 52);
		  		  SSD1306_Puts ("    POR HORA", &Font_7x10, 1);
			  	  SSD1306_GotoXY (7, 40);
		  		  SSD1306_Puts ("Cambiar hora", &Font_7x10, 1);
		  	  }
		  	  SSD1306_UpdateScreen();
		  }
	else if(*menu == 51)	//SELECCIONES MENU PERSIANA
		  {
			  SSD1306_GotoXY (0,0);
			  SSD1306_Puts (" PERSIANA", &Font_11x18, 1);
			  if (*setmodo == 1){
				  SSD1306_GotoXY (7, 16);
				  SSD1306_Puts ("Atras", &Font_7x10, 1);
				  SSD1306_GotoXY (7, 28);
				  SSD1306_Puts ("Cambiar", &Font_7x10, 1);
				  SSD1306_GotoXY (7, 40);
				  SSD1306_Puts (" Apertura: 80%", &Font_7x10, 1);
				  SSD1306_GotoXY (7, 52);
				  SSD1306_Puts ("   Cierre: 20%", &Font_7x10, 1);
			  }
			  if (*setmodo == 2){
				  sprintf(buffer, ">  %02d:%02d", (int)  h_apertura[0], (int)h_apertura[1]);
				  SSD1306_GotoXY (7, 16);
				  SSD1306_Puts ("Atras", &Font_7x10, 1);
				  SSD1306_GotoXY (7, 28);
				  SSD1306_Puts ("Cambiar", &Font_7x10, 1);
				  SSD1306_GotoXY (7, 40);
				  SSD1306_Puts (buffer, &Font_7x10, 1);
				  sprintf(buffer, ">  %02d:%02d", (int)  h_cierre[0], (int)h_cierre[1]);
				  SSD1306_GotoXY (7, 52);
				  SSD1306_Puts (buffer, &Font_7x10, 1);
			  }
			  SSD1306_UpdateScreen();
		  }
	else if(*menu == 52)	//CONFIGURACION APERTURA PERSIANA
		  {
			  SSD1306_GotoXY (0,0);
			  SSD1306_Puts (" PERSIANA", &Font_11x18, 1);
			  if (*setmodo == 1){	//MODO LUZ
				  SSD1306_GotoXY (7, 16);
				  SSD1306_Puts ("Cambiar apertura", &Font_7x10, 1);
			  	  sprintf(buffer, ">    %d%%", (int)(TIM1->CNT)/4*10);
				  SSD1306_GotoXY (7, 28);
				  SSD1306_Puts (buffer, &Font_11x18, 1);
				  SSD1306_GotoXY (7, 52);
				  SSD1306_Puts ("     20%", &Font_7x10, 1);
			  }
			  if (*setmodo == 2){	//MODO HORA
				  SSD1306_GotoXY (7, 16);
				  SSD1306_Puts ("Cambiar apertura", &Font_7x10, 1);

				  int resto = TIM1->CNT % 8;
				  if (resto == 0)
					  resto = 0;
				  else
					  resto = 30;

				  h_apertura[0]=(TIM1->CNT)/8;
				  h_apertura[1]=resto;
			  	  sprintf(buffer, ">  %02d:%02d", (int)(TIM1->CNT)/8, resto);
				  SSD1306_GotoXY (7, 28);
				  SSD1306_Puts (buffer, &Font_11x18, 1);
				  SSD1306_GotoXY (7, 52);
				  sprintf(buffer, ">  %02d:%02d", (int)  h_cierre[0], (int)h_cierre[1]);
				  SSD1306_Puts (buffer, &Font_7x10, 1);
			  }
			  SSD1306_UpdateScreen();
		  }
	else if(*menu == 53)	//CONFIGURACION CIERRE PERSIANA
		  {
		  SSD1306_GotoXY (0,0);
		  SSD1306_Puts (" PERSIANA", &Font_11x18, 1);
		  if (*setmodo == 1){	//MODO LUZ
			  SSD1306_GotoXY (7, 16);
			  SSD1306_Puts ("Cambiar cierre", &Font_7x10, 1);
			  SSD1306_GotoXY (7, 28);
			  SSD1306_Puts ("     80%", &Font_7x10, 1);
		  	  sprintf(buffer, ">    %d%%", (int)(TIM1->CNT)/4*10);
			  SSD1306_GotoXY (7, 40);
			  SSD1306_Puts (buffer, &Font_11x18, 1);
		  }
		  if (*setmodo == 2){	//MODO HORA
			  SSD1306_GotoXY (7, 16);
			  SSD1306_Puts ("Cambiar cierre", &Font_7x10, 1);

			  int resto = TIM1->CNT % 8;
			  if (resto == 0)
				  resto = 0;
			  else
				  resto = 30;

			  h_cierre[0]=(TIM1->CNT)/8;
			  h_cierre[1]=resto;
			  sprintf(buffer, ">  %02d:%02d", (int)  h_apertura[0], (int)h_apertura[1]);
			  SSD1306_GotoXY (7, 28);
			  SSD1306_Puts (buffer, &Font_7x10, 1);
		  	  sprintf(buffer, ">  %02d:%02d", (int)(TIM1->CNT)/8, resto);
			  SSD1306_GotoXY (7, 40);
			  SSD1306_Puts (buffer, &Font_11x18, 1);
		  }
		  SSD1306_UpdateScreen();
		  }
	else if(*menu == 6)	//MENU RIEGO
		  {
			  sprintf(buffer, "%d%%", *sethum*100/255);
		  	  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("   RIEGO", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Atras", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 28);
		  	  SSD1306_Puts ("Cambiar humedad", &Font_7x10, 1);
		  	  SSD1306_GotoXY (45, 40);
		  	  SSD1306_Puts (buffer, &Font_11x18, 1);
		  	  SSD1306_UpdateScreen();
		  }
	else if(*menu == 61)	//SELECCION RIEGO
		  {
			  sprintf(buffer, "%d%%", (int)((TIM1->CNT)/4*100/17));
			  *sethum = (TIM1->CNT)/4*15;
			  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("RIEGO", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Pulse para", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 25);
		  	  SSD1306_Puts ("confirmar", &Font_7x10, 1);
		  	  SSD1306_GotoXY (40, 37);
		  	  SSD1306_Puts (buffer, &Font_16x26, 1);
		  	  SSD1306_UpdateScreen();
		  }
	else	//FALLO DE MENU
		  {
		  	  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("  FALLO EN MENU", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Pulse para", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 28);
		  	  SSD1306_Puts ("reiniciar", &Font_7x10, 1);
		  	  SSD1306_UpdateScreen();
		  }
}

/******************** CAMBIAR DE MENU ********************/
void CallbackMenu(volatile uint8_t* menu, float temp, uint8_t* setcal, uint8_t* setvent, uint8_t* sethum, uint8_t* setmodo, uint8_t* setalarm)
{
	/* EL REGISTRO ARR CAMBIA EL PRESCALER AL NECESARIO PARA CADA MENU */

	if (*menu == 100)	//MENU STANDBY
	{
		*menu = 0;	//MENU PRINCIPAL
		  TIM1->ARR = 23;
		TIM1->CNT = 0;
		standby = HAL_GetTick();
	}
	else if(*menu == 0)	//MENU PRINCIPAL
	{
		switch(TIM1->CNT){
		case 0:
			*menu = 1;	//MENU DE TEMPERATURA
			  TIM1->ARR = 0;
			break;
		case 4:
			*menu = 2;	//MENU DE CALEFACCION
			  TIM1->ARR = 7;
			break;
		case 8:
			*menu = 3;	//MENU DE VENTILADOR
			  TIM1->ARR = 7;
			break;
		case 12:
			*menu = 4;	//MENU DE ALARMA
			  TIM1->ARR = 7;
			break;
		case 16:
			*menu = 5;	//MENU PERSIANA
			if (*setmodo == 0)
				  TIM1->ARR = 7;
			else
				  TIM1->ARR = 11;
			TIM1->CNT = 0;
			break;
		case 20:
			*menu = 6;	//MENU DE RIEGO
			  TIM1->ARR = 7;
			break;
		}
		TIM1->CNT = 0;
	}
	else if (*menu == 1)	//MENU TEMPERATURA
	{
		*menu = 0;	//MENU PRINCIPAL
		  TIM1->ARR = 23;
		TIM1->CNT = 0;
		standby = HAL_GetTick();
	}
	else if (*menu == 2)	//MENU CALEFACCION
	{
		switch(TIM1->CNT){
		case 0:
			*menu = 0;
			  TIM1->ARR = 23;
			TIM1->CNT = 4;
			standby = HAL_GetTick();
			break;
		case 4:
			*menu = 21;
			  TIM1->ARR = 163;
			TIM1->CNT = *setcal*4;
			break;
		}

	}
	else if (*menu == 21)	//SELECCION CALEFACCION
	{
		*menu = 2;
		  TIM1->ARR = 7;
		TIM1->CNT = 0;
	}
	else if (*menu == 3)	//MENU VENTILADOR
	{
		switch(TIM1->CNT){
		case 0:
			*menu = 0;
			  TIM1->ARR = 23;
			TIM1->CNT = 8;
			standby = HAL_GetTick();
			break;
		case 4:
			*menu = 31;
			  TIM1->ARR = 123;
			TIM1->CNT = *setvent*4;
			break;
		}
	}
	else if (*menu == 31)	//SELECCION VENTILADOR
	{
		*menu = 3;
		  TIM1->ARR = 7;
		TIM1->CNT = 0;
	}
	else if (*menu == 4)	//MENU ALARMA
	{
		switch(TIM1->CNT){
		case 0:
			*menu = 0;
			  TIM1->ARR = 23;
			TIM1->CNT = 12;
			standby = HAL_GetTick();
			break;
		case 4:
			if (*setalarm == 0)
				*setalarm = 1;
			else
				*setalarm = 0;
			break;
		}
	}
	else if (*menu == 5)	//MENU PERSIANA
	{
		switch(TIM1->CNT){
		case 0:
			*menu = 0;
			  TIM1->ARR = 23;
			TIM1->CNT = 16;
			standby = HAL_GetTick();
			break;
		case 4:
			if (*setmodo < 2)
				*setmodo = *setmodo + 1;
			else
				*setmodo = 0;
			if (*setmodo == 0)
				  TIM1->ARR = 7;
			else
				  TIM1->ARR = 11;
			break;
		case 8:
			*menu = 51;
				  TIM1->ARR = 7;
			TIM1->CNT = 0;
			break;
		}
	}
	else if (*menu == 51)	//MENU SELECCION MODO PERSIANA
	{
		switch(TIM1->CNT){
		case 0:
			*menu = 5;
			if (*setmodo == 0)
				  TIM1->ARR = 7;
			else
				  TIM1->ARR = 11;
			TIM1->CNT = 0;
			break;
		case 4:
			if (*setmodo == 1){
				*menu = 52;
				  TIM1->ARR = 43;
				TIM1->CNT = 0;
			}
			else{
				*menu = 52;
				  TIM1->ARR = 191;
				TIM1->CNT = 0;
			}
			break;
		}
	}
	else if (*menu == 52)	//CONFIGURACION APERTURA PERSIANA
	{
		*menu = 53;
		if (*setmodo == 0)
			  TIM1->ARR = 7;
		else if (*setmodo == 1)
			  TIM1->ARR = 43;
		else if (*setmodo == 2)
			  TIM1->ARR = 191;
		TIM1->CNT = 0;
	}
	else if (*menu == 53)	//CONFIGURACION CIERRE PERSIANA
	{
		*menu = 51;
		TIM1->ARR = 7;
		TIM1->CNT = 0;
	}
	else if (*menu == 6)	//MENU RIEGO
	{
		switch(TIM1->CNT){
		case 0:
			*menu = 0;
			  TIM1->ARR = 23;
			TIM1->CNT = 20;
			standby = HAL_GetTick();
			break;
		case 4:
			*menu = 61;
			  TIM1->ARR = 71;
			TIM1->CNT = *sethum*4/15;
			break;
		}
	}
	else if (*menu == 61)	//SELECCION RIEGO
	{
		*menu = 6;
		  TIM1->ARR = 7;
		TIM1->CNT = 0;
	}
	else
	{
		*menu = 0;
		  TIM1->ARR = 23;
		TIM1->CNT = 0;
		standby = HAL_GetTick();
	}
}
