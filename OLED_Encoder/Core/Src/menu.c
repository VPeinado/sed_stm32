/*
 * menu.c
 *
 *  Created on: Dec 18, 2020
 *      Author: Victor Peinado
 */

#include "stm32f4xx_hal.h";
#include "menu.h"
#include "fonts.h"
#include "ssd1306.h"
#include "ds1307.h"
#include "stdio.h"
#include "string.h"

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim4;
uint32_t standby = 0;
uint32_t tick_anterior = 0;

/******************** DIBUJAR LA FLECHA EN LOS MENUS DESEADOS ********************/
void DrawArrow(volatile uint8_t menu)
{
	if(menu == 21 || menu == 31 || menu == 100)
	{
	  	  SSD1306_GotoXY (0, 16);
	  	  SSD1306_DrawFilledRectangle(0, 0, 7, 36, SSD1306_COLOR_BLACK);
	}
	else
	{
		switch(TIM1->CNT){	//MOVIMIENTO FLECHITA
		  case 0:
		  	  SSD1306_GotoXY (0, 16);
		  	  SSD1306_Puts (">", &Font_7x10, 1);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
			  break;
		  case 4:
		  	  SSD1306_GotoXY (0, 28);
		  	  SSD1306_Puts (">", &Font_7x10, 1);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
			  break;
		  case 8:
		  	  SSD1306_GotoXY (0, 40);
		  	  SSD1306_Puts (">", &Font_7x10, 1);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
			  break;
		  case 12:
		  	  SSD1306_GotoXY (0, 52);
		  	  SSD1306_Puts (">", &Font_7x10, 1);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
		  	  SSD1306_DrawFilledRectangle(0, 0, 7, 11, SSD1306_COLOR_BLACK);
			  break;
		  }
	}
}

/******************** DEVOLVER LA FLECHA A TICK CORRECTO DESPUES DE UN TIEMPO ********************/
void DebounceTick(volatile uint8_t menu)
{
	uint32_t tiempo_anterior = 0;
	int resto = TIM1->CNT % 4;
	int cociente = TIM1->CNT / 4;

	if (resto == 1 || resto == 2 || resto == 3){
		  if (HAL_GetTick() - tiempo_anterior > 600){	//Periodo antirrebotes
			  if (resto == 1 || resto == 2){
				  TIM1->CNT = 4 * cociente;
				  tiempo_anterior = HAL_GetTick();
			  }
			  else if (resto == 3)	//|| resto == 2, al estar en medio se puede cambiar
			  {
				  TIM1->CNT = 4 * cociente + 4;
				  tiempo_anterior = HAL_GetTick();
			  }
			  if (TIM1->CNT > TIM1->ARR){
				  TIM1->CNT = 0;
				  tiempo_anterior = HAL_GetTick();
			  }
		  }
	}
	else
		  tiempo_anterior = HAL_GetTick();
}

/******************** TIEMPO DE STANDBY ********************/
void Standby(volatile uint8_t* menu)
{
	if (tick_anterior == TIM1->CNT)
	{
		if (HAL_GetTick() - standby > 5000)	//Periodo standby
		{
			*menu = 100;
			 standby = HAL_GetTick();
		}
	}
	else
	{
		standby = HAL_GetTick();
	}
	tick_anterior = TIM1->CNT;
}

/******************** DIBUJAR EL MENU ********************/
void DrawMenu(volatile uint8_t* menu, uint8_t* rtc_info, uint8_t* temp, uint8_t* setcal, uint8_t* setvent, uint8_t setalarm)
{
	  SSD1306_DrawFilledRectangle(0, 0, 127, 63, SSD1306_COLOR_BLACK);	//REFRESCA LA PANTALLA SIN CLEAR

	  DebounceTick(*menu);	//LLAMADA POR SI TICK INCORRECTO
	  DrawArrow(*menu);	//LLAMADA PARA DIBUJAR LA FLECHITA

	  char buffer[50];
	  char dia[15];

	if(*menu == 100)	//MENU STANDBY
	{
		switch(*&rtc_info[3]) //TRANSFORMACION DE week_day EN STRING
		{
			case 1:
				sprintf(dia, "   Lunes");
				break;
			case 2:
				sprintf(dia, "  Martes");
				break;
			case 3:
				sprintf(dia, " Miercoles");
				break;
			case 4:
				sprintf(dia, "  Jueves");
				break;
			case 5:
				sprintf(dia, "  Viernes");
				break;
			case 6:
				sprintf(dia, "  Sabado");
				break;
			case 7:
				sprintf(dia, "  Domingo");
				break;
			default:
				sprintf(dia, "ERROR");
				break;
		}
	  	  SSD1306_GotoXY (18, 0);
		  sprintf(buffer, "%02d:%02d:%02d", *&rtc_info[0], *&rtc_info[1], *&rtc_info[2]);
	  	  SSD1306_Puts (buffer, &Font_11x18, 1);
	  	  SSD1306_GotoXY (0, 25);
	  	  SSD1306_Puts (dia, &Font_11x18, 1);
	  	  SSD1306_GotoXY (8, 45);
		  sprintf(buffer, "%02d/%02d/20%02d", *&rtc_info[4], *&rtc_info[5], *&rtc_info[6]);
	  	  SSD1306_Puts (buffer, &Font_11x18, 1);
	  	  SSD1306_UpdateScreen();
	}
	else if(*menu == 0)	//MENU PRINCIPAL
		  {
		  	  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts (" PRINCIPAL", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Temperatura", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 28);
		  	  SSD1306_Puts ("Calefaccion", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 40);
		  	  SSD1306_Puts ("Ventilador", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 52);
		  	  SSD1306_Puts ("Alarma", &Font_7x10, 1);
		  	  SSD1306_UpdateScreen();

			  Standby(menu);
		  }
	else if(*menu == 1)	//MENU TEMPERATURA
		  {
			  sprintf(buffer, "%d'C", *temp);
		  	  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("TEMPERATURA", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Atras", &Font_7x10, 1);
		  	  SSD1306_GotoXY (35, 32);
		  	  SSD1306_Puts (buffer, &Font_16x26, 1);
		  	  SSD1306_UpdateScreen();
		  }
	else if(*menu == 2)	//MENU CALEFACCION
		  {
			  sprintf(buffer, "%d'C", *setcal);
		  	  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("CALEFACCION", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Atras", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 28);
		  	  SSD1306_Puts ("Cambiar", &Font_7x10, 1);
		  	  SSD1306_GotoXY (42, 40);
		  	  SSD1306_Puts (buffer, &Font_11x18, 1);
		  	  SSD1306_UpdateScreen();
		  }
	else if(*menu == 21)	//SELECCION CALEFACCION
		  {
			  sprintf(buffer, "%d'C", (TIM1->CNT)/4);
			  *setcal = (TIM1->CNT)/4;
		  	  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("CALEFACCION", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Pulse para", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 25);
		  	  SSD1306_Puts ("confirmar", &Font_7x10, 1);
		  	  SSD1306_GotoXY (35, 37);
		  	  SSD1306_Puts (buffer, &Font_16x26, 1);
		  	  SSD1306_UpdateScreen();
		  }
	else if(*menu == 3)	//MENU VENTILADOR
		  {
			  sprintf(buffer, "%d rpm", *setvent*10);
		  	  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("VENTILADOR", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Atras", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 28);
		  	  SSD1306_Puts ("Cambiar", &Font_7x10, 1);
		  	  SSD1306_GotoXY (32, 40);
		  	  SSD1306_Puts (buffer, &Font_11x18, 1);
		  	  SSD1306_UpdateScreen();
		  }
	else if(*menu == 31)	//SELECCION VENTILADOR
		  {
			  sprintf(buffer, "%d rpm", ((TIM1->CNT)/4)*10);
			  *setvent = (TIM1->CNT)/4;
			  TIM4->CCR1 = *setvent;
			  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("VENTILADOR", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Pulse para", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 25);
		  	  SSD1306_Puts ("confirmar", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 37);
		  	  SSD1306_Puts (buffer, &Font_16x26, 1);
		  	  SSD1306_UpdateScreen();
		  }
	else if(*menu == 4)	//MENU ALARMA
		  {
		  	  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("  ALARMA", &Font_11x18, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Atras", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 28);
		  	  SSD1306_Puts ("Cambiar", &Font_7x10, 1);
		  	  SSD1306_GotoXY (3, 45);
		  	  if (setalarm == 0)
		  		  SSD1306_Puts ("DESACTIVADA", &Font_11x18, 1);
		  	  else
		  		  SSD1306_Puts (" ACTIVADA", &Font_11x18, 1);
		  	  SSD1306_UpdateScreen();
		  }
	else	//FALLO DE MENU
		  {
		  	  SSD1306_GotoXY (0,0);
		  	  SSD1306_Puts ("  FALLO EN MENU", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 16);
		  	  SSD1306_Puts ("Pulse para", &Font_7x10, 1);
		  	  SSD1306_GotoXY (7, 28);
		  	  SSD1306_Puts ("reiniciar", &Font_7x10, 1);
		  	  SSD1306_UpdateScreen();
		  }
}

/******************** CAMBIAR DE MENU ********************/
void CallbackMenu(volatile uint8_t* menu, uint8_t* temp, uint8_t* setcal, uint8_t* setvent, uint8_t* setalarm)
{
	/* EL REGISTRO ARR CAMBIA EL PRESCALER AL NECESARIO PARA CADA MENU */

	if (*menu == 100)	//MENU STANDBY
	{
		*menu = 0;	//MENU PRINCIPAL
		  TIM1->ARR = 15;
		TIM1->CNT = 0;
	}
	else if(*menu == 0)	//MENU PRINCIPAL
	{
		switch(TIM1->CNT){
		case 0:
			*menu = 1;	//MENU DE TEMPERATURA
			  TIM1->ARR = 0;
			break;
		case 4:
			*menu = 2;	//MENU DE CALEFACCION
			  TIM1->ARR = 7;
			break;
		case 8:
			*menu = 3;	//MENU DE VENTILADOR
			  TIM1->ARR = 7;
			break;
		case 12:
			*menu = 4;	//MENU DE ALARMA
			  TIM1->ARR = 7;
			break;
		}
		TIM1->CNT = 0;
	}
	else if (*menu == 1)	//MENU TEMPERATURA
	{
		*menu = 0;	//MENU PRINCIPAL
		  TIM1->ARR = 15;
		TIM1->CNT = 0;
	}
	else if (*menu == 2)	//MENU CALEFACCION
	{
		switch(TIM1->CNT){
		case 0:
			*menu = 0;
			  TIM1->ARR = 15;
			TIM1->CNT = 0;
			break;
		case 4:
			*menu = 21;
			  TIM1->ARR = 163;
			TIM1->CNT = *setcal*4;
			break;
		}

	}
	else if (*menu == 21)	//SELECCION CALEFACCION
	{
		*menu = 2;
		  TIM1->ARR = 7;
		TIM1->CNT = 0;
	}
	else if (*menu == 3)	//MENU CALEFACCION
	{
		switch(TIM1->CNT){
		case 0:
			*menu = 0;
			  TIM1->ARR = 15;
			TIM1->CNT = 0;
			break;
		case 4:
			*menu = 31;
			  TIM1->ARR = 123;
			TIM1->CNT = *setvent*4;
			break;
		}

	}
	else if (*menu == 31)	//SELECCION CALEFACCION
	{
		*menu = 3;
		  TIM1->ARR = 7;
		TIM1->CNT = 0;
	}
	else if (*menu == 4)	//MENU ALARMA
	{
		switch(TIM1->CNT){
		case 0:
			*menu = 0;
			  TIM1->ARR = 15;
			TIM1->CNT = 0;
			break;
		case 4:
			if (*setalarm == 0)
				*setalarm = 1;
			else
				*setalarm = 0;
			break;
		}
	}
	else
	{
		*menu = 0;
		  TIM1->ARR = 15;
		TIM1->CNT = 0;
	}
}
