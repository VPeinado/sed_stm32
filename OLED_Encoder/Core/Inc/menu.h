/*
 * menu.h
 *
 *  Created on: Dec 18, 2020
 *      Author: Victor Peinado
 */

#ifndef INC_MENU_H_
#define INC_MENU_H_

void DrawMenu(volatile uint8_t* menu, uint8_t* rtc_info, uint8_t* temp, uint8_t* setcal, uint8_t* setvent, uint8_t setalarm);
void DrawArrow(volatile uint8_t);
void CallbackMenu(volatile uint8_t* menu, uint8_t* temp, uint8_t* setcal, uint8_t* setvent, uint8_t* setalarm);
void DebounceTick(volatile uint8_t menu);
void Standby(volatile uint8_t* menu);

#endif /* INC_MENU_H_ */
