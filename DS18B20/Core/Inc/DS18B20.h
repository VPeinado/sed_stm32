/*
 * DS18B20.h
 *
 *  Created on: Dec 18, 2020
 *      Author: Luis Quintana
 */

#ifndef INC_DS18B20_H_
#define INC_DS18B20_H_

float get_Temperature();


#endif /* INC_DS18B20_H_ */
