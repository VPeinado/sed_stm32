/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "ssd1306.h"
#include "test.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */
  SSD1306_Init();  // initialise  //7x10  11x18  16x26

  /*SSD1306_GotoXY (0,0);
  SSD1306_Puts ("  Menu 1", &Font_11x18, 1);
  //SSD1306_GotoXY (0, 16);
  //SSD1306_Puts (">", &Font_7x10, 1);
  SSD1306_GotoXY (7, 16);
  SSD1306_Puts ("Submenu 1", &Font_7x10, 1);
  SSD1306_GotoXY (7, 28);
  SSD1306_Puts ("Submenu 2", &Font_7x10, 1);
  SSD1306_GotoXY (7, 40);
  SSD1306_Puts ("Submenu 3", &Font_7x10, 1);
  SSD1306_GotoXY (7, 52);
  SSD1306_Puts ("Submenu 4", &Font_7x10, 1);
  SSD1306_UpdateScreen(); //display*/

  int i;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  /*char cadena1[] = "1";
	  char cadena2[] = "2";
	  char cadena3[] = "3";
	  char cadena4[] = "4";
	  char cadena5[] = "5";*/


	  for(i=16; i<=52; i=i+12){
		  //SSD1306_Clear();
	  	  SSD1306_GotoXY (0, i);
	  	  SSD1306_Puts (">", &Font_7x10, 1);
	  	  SSD1306_DrawFilledRectangle(0, i+12, 7, 11, SSD1306_COLOR_BLACK);
	  	  SSD1306_DrawFilledRectangle(0, i-12, 7, 11, SSD1306_COLOR_BLACK);
	  	  SSD1306_GotoXY (0,0);
	  	  SSD1306_Puts ("MENU 1", &Font_11x18, 1);
	  	  SSD1306_GotoXY (7, 16);
	  	  SSD1306_Puts ("Submenu 1", &Font_7x10, 1);
	  	  SSD1306_GotoXY (7, 28);
	  	  SSD1306_Puts ("Submenu 2", &Font_7x10, 1);
	  	  SSD1306_GotoXY (7, 40);
	  	  SSD1306_Puts ("Submenu 3", &Font_7x10, 1);
	  	  SSD1306_GotoXY (7, 52);
	  	  SSD1306_Puts ("Submenu 4", &Font_7x10, 1);
	  	  SSD1306_UpdateScreen();
	  	  HAL_Delay(1000);
	  }
	  for(i=52; i>=16; i=i-12){
		  //SSD1306_Clear();
//ROTACION DE LA FLECHITA
	  	  SSD1306_GotoXY (0, i);
	  	  SSD1306_Puts (">", &Font_7x10, 1);
	  	  //CODIGO PARA NO HACER CLEAR
	  	  SSD1306_DrawFilledRectangle(0, i+12, 7, 11+12, SSD1306_COLOR_BLACK);
	  	  SSD1306_DrawFilledRectangle(0, i-12, 7, 11, SSD1306_COLOR_BLACK);
	  	  ///////////////////////////
	  	  SSD1306_GotoXY (0,0);
	  	  SSD1306_Puts ("MENU 1", &Font_11x18, 1);
	  	  SSD1306_GotoXY (7, 16);
	  	  SSD1306_Puts ("Submenu 1", &Font_7x10, 1);
	  	  SSD1306_GotoXY (7, 28);
	  	  SSD1306_Puts ("Submenu 2", &Font_7x10, 1);
	  	  SSD1306_GotoXY (7, 40);
	  	  SSD1306_Puts ("Submenu 3", &Font_7x10, 1);
	  	  SSD1306_GotoXY (7, 52);
	  	  SSD1306_Puts ("Submenu 4", &Font_7x10, 1);
	  	  SSD1306_UpdateScreen();
	  	  HAL_Delay(1000);
	  }
/*
	  //SSD1306_Clear();
	  SSD1306_GotoXY (0, 28);
	  SSD1306_Puts (">", &Font_7x10, 1);
	  SSD1306_UpdateScreen();
	  HAL_Delay(1000);

	  //SSD1306_Clear();
	  SSD1306_GotoXY (0, 40);
	  SSD1306_Puts (">", &Font_7x10, 1);
	  SSD1306_UpdateScreen();
	  HAL_Delay(1000);

	  //SSD1306_Clear();
	  SSD1306_GotoXY (0, 52);
	  SSD1306_Puts (">", &Font_7x10, 1);
	  SSD1306_UpdateScreen();
	  HAL_Delay(1000);*/
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 400000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
